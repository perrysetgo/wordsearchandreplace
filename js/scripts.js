
  var wordSearch = function (mainInput, searchInput, replaceInput){
  	// /debugger; 
 	var mainInputArray = createMainInputArray(mainInput); 
  for (var i = 0; i < mainInputArray.length; i++) {
    if (mainInputArray[i] == searchInput){

    mainInputArray[i] = replaceInput;
    } 
    else{
          return "I'm sorry, we couldn't find a match. Try again?"
    }

  }
        return joinArray(mainInputArray); 
}

var joinArray = function(mainInputArray){
	var outputText = mainInputArray.toString();
	return outputText.replace(/,/g , " ");
}

var createMainInputArray = function(mainInput){
  return mainInputArray = mainInput.split(" ");
};

$(document).ready(function() {
  $("form#wordSearch").submit(function(event) {
    var mainInput = $("input#mainInput").val(); //add functionality to handle null values
    var searchInput = $("input#searchInput").val();
    var replaceInput = $("input#replaceInput").val();

    var result = wordSearch(mainInput, searchInput, replaceInput);

    $(".answer").text(result);

    $("#result").show();
    event.preventDefault();
  });
});