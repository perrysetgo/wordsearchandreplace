describe('wordSearch', function() {
  it("takes an input and returns a changed input to the screen", function() {
    expect(wordSearch("hello world", "world", "universe")).to.equal("hello universe");
  });
 });

describe('createMainInputArray', function() {
   it("takes an input and transforms it into an array", function() {
    expect(createMainInputArray("hello world")).to.be.instanceof(Array); 
  });
});

describe('joinArray', function() {
   it("takes an array as an input and transforms it into a string, removing the commas", function() {
    expect(joinArray(["hello", "world"])).to.equal("hello world");
  });
});